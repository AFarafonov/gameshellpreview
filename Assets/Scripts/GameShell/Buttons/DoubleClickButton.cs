﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GameShell.Buttons
{
    public class DoubleClickButton : ButtonComponent
    {
        protected Animator Animator;
        protected readonly int AnimateActivate = Animator.StringToHash("Activate");
        protected readonly int AnimateDeactivate = Animator.StringToHash("Deactivate");
        protected int CountClick;
        protected Coroutine TimerCoroutine;
        protected readonly float TargetTime;

        public DoubleClickButton(GameObject parent, GameObject view, Action onClickAction, float time):base(parent, view, onClickAction)
        {
            TargetTime = time;
        }

        protected override void OnInit()
        {
            base.OnInit();

            Animator = View.GetComponent<Animator>();
        }

        protected override void OnClick()
        {
            CountClick ++;

            if (CountClick == 1)
            {
                FirstClick();
                return;
            }

            SecondClick();

            base.OnClick();
        }

        protected virtual void FirstClick()
        {
            Animator.SetTrigger(AnimateActivate);
            TimerCoroutine = GS.Instance.StartCoroutine(Timer.Start(TargetTime, CompleteTimer));
            GS.Instance.CallbackUpdate += TapNotOnButton;
        }

        protected virtual void SecondClick()
        {
            GS.Instance.CallbackUpdate -= TapNotOnButton;
            GS.Instance.StopCoroutine(TimerCoroutine);
            CountClick = 0;
         }

        protected void TapNotOnButton()
        {
            if (EventSystem.current.currentSelectedGameObject == null || EventSystem.current.currentSelectedGameObject != View)
            {
                CompleteTimer();
            }
        }

        protected void CompleteTimer()
        {
            CountClick = 0;

            Animator.ResetTrigger(AnimateActivate);
            Animator.SetTrigger(AnimateDeactivate);

            GS.Instance.CallbackUpdate -= TapNotOnButton;
            GS.Instance.StopCoroutine(TimerCoroutine);
        }

        protected override void OnDestroy()
        {
            Animator = null;

            if(TimerCoroutine != null)
                GS.Instance.StopCoroutine(TimerCoroutine);

            GS.Instance.CallbackUpdate -= TapNotOnButton;
        }

        protected override void OnUpdate()
        {
        }
    }
}
