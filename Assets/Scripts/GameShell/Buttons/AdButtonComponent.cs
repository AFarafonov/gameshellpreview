﻿using System;
using GameShell.Core.EventSystem;
using UnityEngine;
using UnityEngine.UI;

namespace GameShell.Buttons
{
    public class AdButtonComponent : ButtonComponent
    {
        private Image _image;
        private Image _backImage;
        private Text _message;

        public AdButtonComponent(GameObject parent, GameObject view, Action onClickAction):base(parent, view, onClickAction)
        {
        }

        protected override void OnInit()
        {
            base.OnInit();

            _image = Transform.FindChild("icon").GetComponent<Image>();
            _backImage = Transform.FindChild("Image").GetComponent<Image>();
            _message = Transform.FindChild("Message").GetComponent<Text>();

            GS.Instance.EventSystem.Attach(GameShellEvents.CompleteAnnounceLoad, OnCompleteAnnounceLoad);
            GS.Instance.EventSystem.Attach(GameShellEvents.ReloadAnnounce, OnReloadAnnounce);

            Hide();
        }

        private void OnReloadAnnounce(EventParams param)
        {
            Hide();
        }

        private void OnCompleteAnnounceLoad(EventParams param)
        {
            _image.overrideSprite = GS.Instance.Service.IconData.SpriteImage;
            _message.text = GS.Instance.Service.IconData.IconText;
            _backImage.color = GS.Instance.Service.IconData.Color;

            Show();
        }

        protected override void OnDestroy()
        {
            _image = null;
            GS.Instance.EventSystem.Detach(GameShellEvents.CompleteAnnounceLoad, OnCompleteAnnounceLoad);
            base.OnDestroy();
        }
    }
}
