﻿using System;
using UnityEngine.UI;

namespace GameShell.Buttons
{
    public class ToggleBehaviour
    {
        protected Action<string, bool> OnClickAction;
        protected Toggle Toggle;
        protected string Type;

        public ToggleBehaviour(string type, Toggle toggle, Action<string, bool> onClickAction, bool isOn)
        {
            Type = type;
            Toggle = toggle;
            OnClickAction = onClickAction;
            Toggle.isOn = isOn;
        }

        public void Init()
        {
            Toggle.onValueChanged.AddListener(OnClick);
        }

        protected virtual void OnClick(bool value)
        {
            if (OnClickAction != null)
                OnClickAction(Type, value);
        }

        public void Destroy()
        {
            Toggle.onValueChanged.RemoveListener(OnClick);
            Toggle = null;
            OnClickAction = null;
        }
    }
}
