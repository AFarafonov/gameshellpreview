﻿using System.IO;
using SimpleJSON;
using UnityEngine;

namespace GameShell
{
    public class SaveManager
    {
        private JSONClass _saveData ;
        public SaveManager()
        {
            LoadSave();
           // Debug.Log(Application.persistentDataPath + "/save.dat");
        }

        public void Save()
        {
            var fileName = Application.persistentDataPath + "/save.dat";
            var file = File.CreateText(fileName);
            file.Write(_saveData.ToString());
            file.Close();
        }

        public void LoadSave()
        {
            var fileName = Application.persistentDataPath + "/save.dat";
            if (!File.Exists(fileName))
            {
               // Debug.Log("create new file");
                _saveData = new JSONClass();
            }
            else
            {
               // Debug.Log("load file");
                var data = File.OpenText(fileName);
                var json = JSON.Parse(data.ReadToEnd());
                _saveData = json.AsObject;
                data.Close();
            }
        }

        public void AddSave(string key, JSONNode data)
        {
            _saveData.Add(key, data);
        }

        public void RemoveSaveItem(string key)
        {
            _saveData.Remove(key);
        }

        public int GetSavedInt(string key)
        {
            var data = _saveData[key];
            return (data != null)? data.AsInt: 0;
        }

        public float GetSavedFloat(string key)
        {
            var data = _saveData[key];
            return (data != null) ? data.AsFloat : 0f;
        }

        public double GetSavedDouble(string key)
        {
            var data = _saveData[key];
            return (data != null) ? data.AsDouble : 0;
        }

        public JSONArray GetSavedArray(string key)
        {
            var data = _saveData[key];
            return (data != null) ? data.AsArray : null;
        }

        public JSONClass GetSavedObject(string key)
        {
            var data = _saveData[key];
            return (data != null) ? data.AsObject : null;
        }

        public JSONNode GetSavedNode(string key)
        {
            var data = _saveData[key];
            return data;
        }

        public bool GetSavedBool(string key)
        {
            var data = _saveData[key];
            return (data != null) ? data.AsBool : false;
        }

        public string GetSavedString(string key)
        {
            var data = _saveData[key];
            return (data != null) ? data.Value : null;
        }
    }
}