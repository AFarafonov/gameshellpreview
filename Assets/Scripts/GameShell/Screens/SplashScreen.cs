﻿using System;
using GameShell.Core;
using UnityEngine;

namespace GameShell.Screens
{
    public class SplashScreen:GuiComponent
    {
        private readonly float _completeTime;
        private float _currentTime;
        private Action _completeAction;
        private bool _start;

        public SplashScreen(GameObject parent, GameObject view, float completeTime):base(parent, view)
        {
            _completeTime = completeTime;
        }

        public void Start(Action completeAction)
        {
            _completeAction = completeAction;
            Init();
            _start = true;
        }

        protected override void OnInit()
        {
            _currentTime = 0;
        }

        protected override void OnDestroy()
        {
            _completeAction = null;
        }

        protected override void OnUpdate()
        {
            if(!_start)return;

            _currentTime += Time.deltaTime;

            if (_currentTime > _completeTime)
            {
                _completeAction();
            }
        }
    }
}
