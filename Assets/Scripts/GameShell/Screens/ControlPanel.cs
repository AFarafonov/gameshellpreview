﻿using System;
using GameShell.Buttons;
using GameShell.Core;
using UnityEngine;

namespace GameShell.Screens
{
    public class ControlPanel : GuiComponent
    {
        private Action _action;
        private ArrowButton _buttonBack;
        private ArrowButton _buttonForward;
        private DoubleClickButton _buttonHome;
        private SettingsGameShell _settings;

        public ControlPanel(GameObject parent, GameObject view, Action action):base(parent, view)
        {
            _action = action;
        }

        protected override void OnInit()
        {
            _settings = GS.Instance.Settings;

            _buttonHome = new DoubleClickButton(View, _settings.ButtonHome, OnClickHome, GS.Instance.Settings.TimeDeactivationButtons);
            _buttonHome.Init();

            if (_settings.EnableArrowButtons)
            {
                _buttonBack = new ArrowButton(ArrowButtonType.Back, View, _settings.ButtonBack, OnClickBack, 2);
                _buttonForward = new ArrowButton(ArrowButtonType.Forward, View, _settings.ButtonForward, OnClickForward, 2);

                _buttonBack.Init();
                _buttonForward.Init();
            }

        }

        private void OnClickHome()
        {
            if (_action != null)
                _action();
        }

        private void OnClickForward()
        {
            GS.Instance.EventSystem.Call(GameShellEvents.ClickForwardArrowButton);
        }

        private void OnClickBack()
        {
            GS.Instance.EventSystem.Call(GameShellEvents.ClickBackArrowButton);
        }

        public override void Show()
        {
            base.Show();
            if (_settings.EnableArrowButtons)
            {
                _buttonBack.Show();
                _buttonForward.Show();
            }
            GS.Instance.EventSystem.Call(GameShellEvents.ShowControlPanel);
        }

        public override void Hide()
        {
            base.Hide();
            if (_settings.EnableArrowButtons)
            {
                _buttonBack.Hide();
                _buttonForward.Hide();
            }
            GS.Instance.EventSystem.Call(GameShellEvents.HideControlPanel);
        }

        protected override void OnDestroy()
        {
            if (_settings.EnableArrowButtons)
            {
                _buttonBack.Destroy();
                _buttonForward.Destroy();
            }
            _buttonHome.Destroy();

            _action = null;
            _settings = null;
        }

        protected override void OnUpdate()
        {
        }
    }
}