﻿using System;
using GameShell.Buttons;
using GameShell.Core;
using UnityEngine;
using GameShell.Helpers;
using GameShell.Dialogs;

namespace GameShell.Screens
{
    public class LockPanel : GuiComponent
    {
        private Action _action;
        private ButtonComponent _buttonReturn;
        private ButtonComponent _buttonLock;
        private ShopDialog shopDialog;

        public LockPanel(GameObject parent, GameObject view, Action returnAction):base(parent, view)
        {
            _action = returnAction;
        }

        protected override void OnInit()
        {
            var _settings = GS.Instance.Settings;

            _buttonReturn = new ButtonComponent(View, _settings.ButtonReturn, OnClickReturn);
            _buttonReturn.Init();

            _buttonLock = new ButtonComponent(View, GS.Instance.Settings.ButtonLock, OnLockButtonClick);
            _buttonLock.Init();
        }

        private void OnLockButtonClick()
        {
            GS.Instance.EventSystem.Call(GameShellEvents.ClickLockButton);
            Destroy();
        }

        private void OnClickReturn()
        {
            if (_action != null)
                _action();
            GS.Instance.EventSystem.Call(GameShellEvents.CloseLockScreen);
            Destroy();
        }

         public override void Show()
        {
            base.Show();
           // GS.Instance.EventSystem.Call(GameShellEvents.ShowControlPanel);
        }

        public override void Hide()
        {
            base.Hide();
           // GS.Instance.EventSystem.Call(GameShellEvents.HideControlPanel);
        }

        protected override void OnDestroy()
        {
            _buttonReturn.Destroy();
            _buttonLock.Destroy();
            _action = null;
        }

        protected override void OnUpdate()
        {
        }
    }
}