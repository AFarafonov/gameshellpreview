﻿using System.Collections.Generic;
using GameShell.Core;
using GameShell.Server;
using UnityEngine;
using UnityEngine.UI;

namespace GameShell.Screens.AdScreen
{
    public class SectionItem : GuiComponent
    {
        private SectionData _sectionData;
        private readonly List<AppItem> _appItems = new List<AppItem>(); 

        public SectionItem(GameObject parent, GameObject view, SectionData data):base(parent, view)
        {
            _sectionData = data;
        }

        protected override void OnInit()
        {
            var header = Transform.FindChild("HeaderText").GetComponent<Text>();
            header.text = _sectionData.Title;

            foreach (ItemAppData data in _sectionData.AppItems)
            {
                var item = new AppItem(View, GS.Instance.Settings.AdIconSection, data, null);
                item.Init();
                _appItems.Add(item);
            }
        }

        protected override void OnDestroy()
        {
            _sectionData = null;

            foreach (var appItem in _appItems)
            {
                appItem.Destroy();
            }
            _appItems.Clear();
        }

        protected override void OnUpdate()
        {
        }
    }
}