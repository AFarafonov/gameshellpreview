﻿using System;
using GameShell.Core;
using GameShell.Helpers;
using GameShell.Server;
using UnityEngine;

namespace GameShell.Screens.AdScreen
{
    public class AppItem : GuiComponent
    {
        private Action _action;
        private ItemAppData _data;
        private AdAppIconHelper _helper;

        public AppItem(GameObject parent, GameObject view, ItemAppData data, Action action):base(parent, view)
        {
            _action = action;
            _data = data;
        }

        protected override void OnInit()
        {
            _helper = View.GetComponent<AdAppIconHelper>();
            _helper.Image.overrideSprite = _data.SpriteImage;
            _helper.Name.text = _data.Title;

            if (_data.Accent != null && GS.Instance.AccentSprites.ContainsKey(_data.Accent))
            {
                var spriteAccent = GS.Instance.AccentSprites[_data.Accent];
                if (spriteAccent != null)
                    _helper.ImageAccent.overrideSprite = spriteAccent;
            }
            else
            {
                _helper.ImageAccent.enabled = false;
            }

            _helper.Button.onClick.AddListener(OnClick);
        }

        private void OnClick()
        {
            if (_data.Url != null)
            {
                Application.OpenURL(_data.Url);
                
                if (_action != null)
                    _action();
            }
        }
      
        protected override void OnDestroy()
        {
            _helper.Button.onClick.RemoveListener(OnClick);

            _helper = null;
            _action = null;
            _data = null;
        }

        protected override void OnUpdate()
        {
        }
    }
}