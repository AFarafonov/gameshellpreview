﻿using System;
using System.Collections.Generic;
using GameShell.Buttons;
using GameShell.Core;
using GameShell.Core.EventSystem;
using GameShell.Helpers;
using GameShell.Server;
using UnityEngine;

namespace GameShell.Screens.AdScreen
{
    public class AdScreen : GuiComponent
    {
        private Action _action;      
        private DoubleClickButton _buttonHome;
        private SettingsGameShell _settings;
        private AdService _service;
        private AdScreenHelper _helper;
        private readonly List<AppItem> _appItems = new List<AppItem>();
        private readonly List<SectionItem> _sectionItems = new List<SectionItem>();

        public AdScreen(AdService service, GameObject parent, GameObject view, Action action):base(parent, view)
        {
            _service = service;
            _action = action;
        }

        protected override void OnInit()
        {
            _helper = View.GetComponent<AdScreenHelper>();

            _settings = GS.Instance.Settings;
            _service = GS.Instance.Service;

            _buttonHome = new DoubleClickButton(View, _settings.ButtonHome, OnClickHome, GS.Instance.Settings.TimeDeactivationButtons);
            _buttonHome.Init();       

            GS.Instance.EventSystem.Attach(GameShellEvents.CompleteAnnounceLoad, OnLoadAnnounce);
            GS.Instance.EventSystem.Attach(GameShellEvents.ReloadAnnounce, OnReLoadAnnounce);
        }

        private void OnReLoadAnnounce(EventParams eventParams)
        {
            foreach (var headerItem in _appItems)
            {
                headerItem.Destroy();
            }
            _appItems.Clear();

            foreach (var sectionItem in _sectionItems)
            {
                sectionItem.Destroy();
            }
            _sectionItems.Clear();
        }

        private void OnLoadAnnounce(EventParams eventParams)
        {
            BuildScreen();
        }

        private void BuildScreen()
        {
            foreach (ItemAppData appData in _service.BigAnnounces)
            {
                var item = new AppItem(_helper.HeaderContent, _settings.AdHeaderPreviewIcon, appData, null);
                item.Init();
                _appItems.Add(item);
            }

            foreach (SectionData data in _service.Sections)
            {
                var item = new SectionItem(_helper.BodyContent, _settings.AdSection, data);
                item.Init();
                _sectionItems.Add(item);
            }
        }

        private void OnClickHome()
        {
            if (_action != null)
                _action();
        }
        /* 
        public override void Show()
        {
            base.Show();
        }

        public override void Hide()
        {
            base.Hide();         
        }
        */
        protected override void OnDestroy()
        {
            _buttonHome.Destroy();

            foreach (var headerItem in _appItems)
            {
                headerItem.Destroy();
            }
            _appItems.Clear();

            foreach (var sectionItem in _sectionItems)
            {
                sectionItem.Destroy();
            }
            _sectionItems.Clear();

            _action = null;
            _settings = null;
            _service = null;
            _helper = null;
        }

        protected override void OnUpdate()
        {
        }
    }
}