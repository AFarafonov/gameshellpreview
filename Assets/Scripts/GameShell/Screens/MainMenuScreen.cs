﻿using System;
using GameShell.Buttons;
using GameShell.Core;
using GameShell.Core.Extensions;
using GameShell.Dialogs;
using UnityEngine;
using Object = UnityEngine.Object;

namespace GameShell.Screens
{
    public class MainMenuScreen:GuiComponent
    {
        private Action _playGameCallback;
        private Action _adCallback;
        private ButtonComponent _buttonPlay;
        private ButtonComponent _buttonSettings;
        private ButtonComponent _buttonHelp;
        private ButtonComponent _buttonAd;

        public MainMenuScreen(GameObject parent, GameObject view, Action playGameCallback, Action adCallback) :base(parent, view)
        {
            _playGameCallback = playGameCallback;
            _adCallback = adCallback;
        }

        protected override void OnInit()
        {
            var settings = GS.Instance.Settings;

            var background = Object.Instantiate(settings.BackgroundMenu);
            var backAnchor = View.transform.FindChild("AnchorBackground").gameObject;
            backAnchor.AddChildUI(background);
            
            _buttonPlay = new ButtonComponent(View, settings.ButtonPlay, OnClickPlay);
            _buttonSettings = new ButtonComponent(View, settings.ButtonSettings, OnClickSettings);
            _buttonHelp = new ButtonComponent(View, settings.ButtonHelp, OnClickHelp);
            _buttonAd = new AdButtonComponent(View, settings.ButtonAd, OnClickAd);

            _buttonPlay.Init();
            _buttonSettings.Init();
            _buttonHelp.Init();
            _buttonAd.Init();
        }

        private void OnClickPlay()
        {
            if (_playGameCallback != null)
                _playGameCallback();
        }

        private void OnClickSettings()
        {
            GS.Instance.ShowSettingsDialog();
        }

        private void OnClickHelp()
        {
            GS.Instance.ShowParentControlDialog(ShowHelpDialog);
        }

        private void ShowHelpDialog()
        {
            var helpDialog = new HelpDialog(GS.Instance.DialogsContainer, GS.Instance.Settings.HelpDialog);
            helpDialog.Init();
        }

        private void OnClickAd()
        {
            _adCallback();
            //GS.Instance.ShowParentControlDialog(_adCallback);
        }

        public override void Show()
        {
            base.Show();
            GS.Instance.EventSystem.Call(GameShellEvents.ShowMainMenu);
        }

        public override void Hide()
        {
            base.Hide();
            GS.Instance.EventSystem.Call(GameShellEvents.HideMainMenu);
        }

        protected override void OnDestroy()
        {
            _buttonPlay.Destroy();
            _buttonSettings.Destroy();
            _buttonHelp.Destroy();
            _buttonAd.Destroy();
        }

        protected override void OnUpdate()
        {
        }
    }
}