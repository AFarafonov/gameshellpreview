﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameShell
{
    [Serializable]
    public struct AccentItem
    {
        public string Id;
        public Sprite Sprite;

        public AccentItem(string id, Sprite sprite)
        {
            Id = id;
            Sprite = sprite;
        }
    }

    [Serializable]
    public class SettingsGameShell:ScriptableObject
    {
        public GameObject SplashScreen;
        public float TimeSplashComplete = 0f;
        public float TimeDeactivationButtons = 0f;
        public bool AnimationSplashComplete = true;

        public GameObject BackgroundMenu;
        public GameObject MainMenu;

        public GameObject ButtonPlay;
        public GameObject ButtonSettings;
        public GameObject ButtonAd;
        public GameObject ButtonHelp;
        public GameObject ButtonForward;
        public GameObject ButtonBack;

        public GameObject ButtonHome;
        public GameObject ButtonLock;
        public GameObject ButtonClose;
        public GameObject ButtonReturn;

        public GameObject ControlPanel;
        public bool EnableArrowButtons = true;

        public GameObject ShopDialog;
        public GameObject SettingsDialog;
        public GameObject HelpDialog;
        public GameObject ParentControlDialog;
        public GameObject BlockerDialog;
        public GameObject LockScreen;

        public string AdURL;
        public GameObject AdScreen;
        public GameObject AdHeaderPreviewIcon;
        public GameObject AdSection;
        public GameObject AdIconSection;
      
        public List<AccentItem> AccentItems = new List<AccentItem>();

        public string PathToLocalizationFile;
        public bool EnableLocalization = true;
    }
}
