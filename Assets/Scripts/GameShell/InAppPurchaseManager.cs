﻿using System;
using UnityEngine;

namespace GameShell
{
    public class InAppPurchaseManager
    {
        public bool IsInitialized;

		private Action _callbackLoadStore;

		private Action _purchaseSuccessCallback;
		private Action _purchaseRestoreCallback;
		private Action<string> _purchaseFailCallback;
		private string _purchaseId;

        public InAppPurchaseManager()
        {
#if UNITY_ANDROID
            //listening for purchase and consume events
            AndroidInAppPurchaseManager.ActionProductPurchased += OnProductPurchased;
            //AndroidInAppPurchaseManager.ActionProductConsumed += OnProductConsumed;
#endif
#if UNITY_IOS
            IOSInAppPurchaseManager.OnTransactionComplete += OnTransactionComplete;
            IOSInAppPurchaseManager.OnRestoreComplete += OnRestoreComplete;
#endif
        }

        private void Init(Action callback)
        {
            if (IsInitialized)
            {
                if (callback != null)
                {
                    callback();
                }
                return;
            }
            //listening for store initilaizing finish
            _callbackLoadStore = callback;
#if UNITY_ANDROID
            AndroidInAppPurchaseManager.ActionBillingSetupFinished += OnBillingConnected;
            AndroidInAppPurchaseManager.Instance.LoadStore();
#endif
#if UNITY_IOS
            IOSInAppPurchaseManager.OnStoreKitInitComplete += OnStoreKitInitComplete;
            IOSInAppPurchaseManager.Instance.LoadStore();
#endif
		}

		private void ClearPurchase()
		{
			_purchaseFailCallback = null;
			_purchaseRestoreCallback = null;
			_purchaseSuccessCallback = null;
			_purchaseId = null;
		}

		private void FirePurchaseFail(string message)
		{
			if (_purchaseFailCallback != null)
			{
				_purchaseFailCallback(message);
				ClearPurchase();
			}
		}

		private void FirePurchaseSuccess()
		{
			if (_purchaseSuccessCallback != null)
			{
				_purchaseSuccessCallback();
				ClearPurchase();
			}
		}

		private void FirePurchaseRestore()
		{
			if (_purchaseRestoreCallback != null)
			{
				_purchaseRestoreCallback();
				ClearPurchase();
			}
		}

		private void MakePurchase()
		{
#if UNITY_ANDROID
			AndroidInAppPurchaseManager.Instance.Purchase(_purchaseId);
#endif
#if UNITY_IOS
			IOSInAppPurchaseManager.Instance.BuyProduct(_purchaseId);
#endif				
		}

		public void Purchase(string id, Action successCallback, Action restoreCallback, Action<string> failCallback)
		{
			_purchaseSuccessCallback = successCallback;
			_purchaseRestoreCallback = restoreCallback;
			_purchaseFailCallback = failCallback;
			_purchaseId = id;
			Init(MakePurchase);
		}

		private void MakeRestore()
		{
#if UNITY_ANDROID
			if (AndroidInAppPurchaseManager.Instance.Inventory.IsProductPurchased(_purchaseId))
			{
				FirePurchaseRestore();
			}
			else
			{
				MakePurchase();
			}
#endif
#if UNITY_IOS
			IOSInAppPurchaseManager.Instance.RestorePurchases();
#endif		
		}

		public void Restore(string id, Action successCallback, Action restoreCallback, Action<string> failCallback)
		{
			_purchaseSuccessCallback = successCallback;
			_purchaseRestoreCallback = restoreCallback;
			_purchaseFailCallback = failCallback;
			_purchaseId = id;
			Init(MakeRestore);
		}

		private void OnStoreKitInitComplete(ISN_Result result)
		{
			IOSInAppPurchaseManager.OnStoreKitInitComplete -= OnStoreKitInitComplete;

			if (result.IsSucceeded)
			{
				IsInitialized = true;

				if (_callbackLoadStore != null)
				{
					_callbackLoadStore();
					_callbackLoadStore = null;
				}
				Debug.Log("StoreKit Init Success. Available products count: " + IOSInAppPurchaseManager.Instance.Products.Count);
			}
			else 
			{				
				Debug.Log("StoreKit Init Failed. Error code: " + result.Error.Code + "\n" + "Error description:" + result.Error.Description);
				FirePurchaseFail(result.Error.Description);
			}
		}

		private void OnBillingConnected(BillingResult result)
		{
			AndroidInAppPurchaseManager.ActionBillingSetupFinished -= OnBillingConnected;

			if (result.isSuccess) {
				//Store connection is Successful. Next we loading product and customer purchasing details
				AndroidInAppPurchaseManager.ActionRetrieveProducsFinished += OnRetriveProductsFinised;
				AndroidInAppPurchaseManager.Instance.RetrieveProducDetails();
				Debug.Log("Billing Connect Success.");
			} 
			else 
			{
				Debug.Log("Billing Connect Faild. " + result.response.ToString() + " " + result.message);
				FirePurchaseFail(result.message);
			}
		}

		private void OnRetriveProductsFinised(BillingResult result)
		{
			AndroidInAppPurchaseManager.ActionRetrieveProducsFinished -= OnRetriveProductsFinised;

			if (result.isSuccess)
			{
				IsInitialized = true;

				if (_callbackLoadStore != null)
				{
					_callbackLoadStore();
					_callbackLoadStore = null;
				}
				Debug.Log("Retrive Products Success. Inventory contains: " + AndroidInAppPurchaseManager.Instance.Inventory.Purchases.Count + " products");
			}
			else 
			{
				Debug.Log("Retrive Products Failed. " + result.response.ToString() + " " + result.message);
				FirePurchaseFail(result.message);
			}
		}

		private void OnTransactionComplete(IOSStoreKitResult result)
		{
			Debug.Log("OnTransactionComplete: " + result.ProductIdentifier);
			Debug.Log("OnTransactionComplete: state: " + result.State);

			switch(result.State) {
			case InAppPurchaseState.Purchased:
				FirePurchaseSuccess();
				Debug.Log ("Transaction was restored.");
				break;
			case InAppPurchaseState.Restored:
				FirePurchaseRestore();
				Debug.Log ("Transaction was successful.");
				break;
			case InAppPurchaseState.Deferred:
				//iOS 8 introduces Ask to Buy, which lets 
				//parents approve any purchases initiated by children
				//You should update your UI to reflect this 
				//deferred state, and expect another Transaction 
				//Complete  to be called again with a new transaction state 
				//reflecting the parent's decision or after the 
				//transaction times out. Avoid blocking your UI 
				//or gameplay while waiting for the transaction to be updated.
				FirePurchaseFail ("Transaction was deffered.");
				Debug.Log ("Transaction was deffered.");
				break;
			case InAppPurchaseState.Failed:
				FirePurchaseFail(result.Error.Description);
				Debug.Log("Transaction failed with error, code: " + result.Error.Code);
				Debug.Log("Transaction failed with error, description: " + result.Error.Description);
				break;
			}
		}

		private void OnRestoreComplete(IOSStoreKitRestoreResult result)
		{
			if (result.IsSucceeded)
			{
				// OnTransactionComplete will fire for each item so don't do anything here
				Debug.Log ("Transaction was restored.");
			}
			else
			{
				FirePurchaseFail(result.Error.Description);
				Debug.Log("Transaction failed with error, code: " + result.Error.Code);
				Debug.Log("Transaction failed with error, description: " + result.Error.Description);
			}
		}

		private void OnProductPurchased(BillingResult result)
		{
			if (result.isSuccess)
			{
				FirePurchaseSuccess();
				Debug.Log("Product Purchase Success " + result.response.ToString() + " " + result.message);
			}
			else if (result.response == 7) // 7 means already bought
			{
				FirePurchaseRestore();
				Debug.Log("Product Purchase Restore " + result.response.ToString() + " " + result.message);
			}
			else
			{
				FirePurchaseFail(result.message);
				Debug.Log("Product Purchase Faile " + result.response.ToString() + " " + result.message);
			}
		}
    }
}
