﻿using System.Collections.Generic;
using GameShell.Buttons;
using GameShell.Core.EventSystem;
using GameShell.Core.Extensions;
using GameShell.Helpers;
using UnityEngine;

namespace GameShell.Dialogs
{
    public class SettingsDialog : Dialog
    {
        private readonly List<ToggleBehaviour> _toggles = new List<ToggleBehaviour>();
        private SettingsDialogHelper _settingsDialogHelper;

        public SettingsDialog(GameObject parent, GameObject view):base(parent, view)
        {
        }

        protected override void OnInit()
        {
            base.OnInit();

            _settingsDialogHelper = View.GetComponent<SettingsDialogHelper>();
           
            SetText();

            _settingsDialogHelper.MusicSlider.normalizedValue = GS.Instance.VolumeMusic;
            _settingsDialogHelper.SoundSlider.normalizedValue = GS.Instance.VolumeSound;

            _settingsDialogHelper.MusicSlider.onValueChanged.AddListener(OnMusicVolumeChange);
            _settingsDialogHelper.SoundSlider.onValueChanged.AddListener(OnSoundVolumeChange);

            GS.Instance.EventSystem.Attach(GameShellEvents.ChangeLangauge, OnChangeLangauge);
            
            foreach (var item in _settingsDialogHelper.Langauges)
            {
                var activ = (GS.Instance.Localization.CurrentLocal == item.LangaugeId);
                var toggle = new ToggleBehaviour(item.LangaugeId, item.Toggle, OnSelect, activ);
                toggle.Init();
                _toggles.Add(toggle);
            }

            GS.Instance.EventSystem.Call(GameShellEvents.ShowSettingsDialog);
        }

        private void SetText()
        {
            if (GS.Instance.Settings.EnableLocalization)
            {
                _settingsDialogHelper.HeaderLangaugeText.text = "header_langauge".ToLocalization();
                _settingsDialogHelper.HeaderMusicText.text = "header_music".ToLocalization();
                _settingsDialogHelper.HeaderSoundText.text = "header_sound".ToLocalization();
            }
        }

        private void OnChangeLangauge(EventParams param)
        {
            SetText();
        }

        private void OnMusicVolumeChange(float value)
        {
            GS.Instance.VolumeMusic = value;
            GS.Instance.EventSystem.Call(GameShellEvents.ChangeVolumeMusic, new VolumeParams(value));
        }

        private void OnSoundVolumeChange(float value)
        {
            GS.Instance.VolumeSound = value;
            GS.Instance.EventSystem.Call(GameShellEvents.ChangeVolumeSound, new VolumeParams(value));
        }

        private void OnSelect(string local, bool value)
        {
            if(!value)return;

            GS.Instance.Localization.ChangeLanguage(local);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            foreach (ToggleBehaviour toggle in _toggles)
            {
                toggle.Destroy();
            }

            _toggles.Clear();

            _settingsDialogHelper.MusicSlider.onValueChanged.RemoveListener(OnMusicVolumeChange);
            _settingsDialogHelper.SoundSlider.onValueChanged.RemoveListener(OnSoundVolumeChange);
            _settingsDialogHelper = null;

            GS.Instance.EventSystem.Detach(GameShellEvents.ChangeLangauge, OnChangeLangauge);

            GS.Instance.EventSystem.Call(GameShellEvents.HideSettingsDialog);
        }
    }
}