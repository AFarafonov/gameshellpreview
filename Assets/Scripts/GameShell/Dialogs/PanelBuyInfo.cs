﻿
using System;
using System.Collections;
using GameShell.Core.Extensions;
using GameShell.Helpers;
using UnityEngine;

namespace GameShell.Dialogs
{
    public class PanelBuyInfo
    {
        private GameObject _buyInfo;
        private GameObject _connectingView;
        private GameObject _errorPanel;
        private GameObject _successView;
        private BuyInfoPanelHelper _helper;
        private ErrorPanelHelper _errorHelper;
        private Item _currentItem;

        public PanelBuyInfo(GameObject buyInfo, GameObject connecting, GameObject notInternet, GameObject success)
        {
            _buyInfo = buyInfo;
            _connectingView = connecting;
            _errorPanel = notInternet;
            _successView = success;
        }

        public void Init()
        {
            _helper = _buyInfo.GetComponent<BuyInfoPanelHelper>();

            _helper.BuyButton.onClick.AddListener(ClickBuy);
            _helper.RestoreButton.onClick.AddListener(ClickRestore);
         
            _errorHelper = _errorPanel.GetComponent<ErrorPanelHelper>();

            _buyInfo.SetActive(false);
            _connectingView.SetActive(false);
            _errorPanel.SetActive(false);
            _successView.SetActive(false);


            _helper.BuyButton.interactable = false;
            _helper.RestoreButton.interactable = false;
            EnableButtons();
        }

        private void EnableButtons()
        {
            _helper.BuyButton.interactable = true;
            _helper.RestoreButton.interactable = true;
        }

        private IEnumerator CheckInternetConnection(Action<bool> action)
        {
			WWW www = new WWW("http://www.apple.com/");
            yield return www;
            if (www.error != null)
            {
                action(false);
            }
            else {
                action(true);
            }
        }

        private void ClickBuy()
        {
            _connectingView.SetActive(true);

            _buyInfo.SetActive(false);
           _errorPanel.SetActive(false);
            _successView.SetActive(false);

            GS.Instance.StartCoroutine(CheckInternetConnection(ResultCheckBuy));
        }

        private void ClickRestore()
        {
            _connectingView.SetActive(true);

            _buyInfo.SetActive(false);
            _errorPanel.SetActive(false);
            _successView.SetActive(false);

            GS.Instance.StartCoroutine(CheckInternetConnection(ResultCheckRestore));
        }

        private void ResultCheckBuy(bool isConnected)
        {
            if (isConnected)
            {
				GS.Instance.InAppPurchaseManager.Purchase(_currentItem.IdItem, OnSuccess, OnRestore, OnFail);
            }
            else
            {
                 _errorPanel.SetActive(true);
                _errorHelper.ErrorText.text = "Error2".ToLocalization();

                _buyInfo.SetActive(false);
                _connectingView.SetActive(false);
                _successView.SetActive(false);
            }
        }

        private void ResultCheckRestore(bool isConnected)
        {
            if (isConnected)
            {
				GS.Instance.InAppPurchaseManager.Restore(_currentItem.IdItem, OnSuccess, OnRestore, OnFail);
            }
            else
            {
                _errorPanel.SetActive(true);
                _errorHelper.ErrorText.text = "Error2".ToLocalization();

                _buyInfo.SetActive(false);
                _connectingView.SetActive(false);
                _successView.SetActive(false);
            }
		}

		private void ShowPopup(string message)
		{
#if UNITY_ANDROID
			AndroidMessage.Create("Show", message);
#endif
#if UNITY_IOS
			IOSNativePopUpManager.showMessage("Show", message);
#endif
			Debug.Log("Show " +  message);
		}

		private void OnSuccess()
		{
            // сохранить в настройках isFullVersion = true;
            // показать сообщение про то, что игра успешно куплена
            //ShowPopup("PANEL SUCCESS!");
            _buyInfo.SetActive(false);
            _connectingView.SetActive(false);
            _errorPanel.SetActive(false);

            _successView.SetActive(true);
            GS.Instance.EventSystem.Call(GameShellEvents.PurchaseCompleted);
  	}

		private void OnRestore()
		{
            // сохранить в настройках isFullVersion = true;
            // показать сообщение про то, что полная версия была восстановлена
            //ShowPopup("PANEL RESTORE!");
            _buyInfo.SetActive(false);
            _connectingView.SetActive(false);
            _errorPanel.SetActive(false);

            _successView.SetActive(true);
            GS.Instance.EventSystem.Call(GameShellEvents.PurchaseCompleted);
            Hide();
		}

		private void OnFail(string message)
		{
            // isFullVersion трогать не нужно
            // показать сообщение об ошибке, в нем можно выводить текст ошибки message, не очень крупно лучше
            //ShowPopup("Error:" + message);
            _buyInfo.SetActive(false);
            _connectingView.SetActive(false);
            _successView.SetActive(false);

            _errorPanel.SetActive(true);
            _errorHelper.ErrorText.text = message;
        }

        public void Show(Item item)
        {
            _currentItem = item;
            _buyInfo.SetActive(true);
        }

        public void Hide()
        {
            _buyInfo.SetActive(false);
        }

    }
}
