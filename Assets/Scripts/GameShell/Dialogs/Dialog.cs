﻿using DG.Tweening;
using GameShell.Buttons;
using GameShell.Core;
using UnityEngine;

namespace GameShell.Dialogs
{
    public class Dialog : GuiComponent
    {
        protected DoubleClickButton ButtonClose;
        protected bool ShowBlocker;

        public Dialog(GameObject parent, GameObject view, bool showBlocker = true):base(parent, view)
        {
            ShowBlocker = showBlocker;
        }

        protected override void OnInit()
        {
            var settings = GS.Instance.Settings;

            var anchor = View.transform.FindChild("AnchorBtnClose").gameObject;
            ButtonClose = new DoubleClickButton(anchor, settings.ButtonClose, OnClickClose, settings.TimeDeactivationButtons);
            ButtonClose.Init();

            RectTransform.localScale = Vector3.zero;
           
            AnimationShow();
            GS.Instance.EventSystem.Call(GameShellEvents.PanelAppear);


            if (ShowBlocker)
            {
                GS.Instance.Blocker.Show();
            }
        }

        private void AnimationShow()
        {
            RectTransform.DOScale(Vector3.one, 0.2f).OnComplete(OnCompleteAnimationShow);
        }

        private void AnimationHide()
        {
            RectTransform.DOScale(Vector3.zero, 0.2f).OnComplete(OnCompleteAnimationHide);
        }

        private void OnClickClose()
        {
            if (ShowBlocker)
            {
                GS.Instance.Blocker.Hide();
            }

            AnimationHide();
        }

        protected virtual void OnCompleteAnimationShow()
        {
        }

        protected virtual void OnCompleteAnimationHide()
        {
             Destroy();
        }

        protected override void OnDestroy()
        {
            GS.Instance.Blocker.Hide();
            ButtonClose.Destroy();
        }

        protected override void OnUpdate()
        {
        }
    }
}