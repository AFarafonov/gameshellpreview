﻿using System;
using GameShell.Buttons;
using GameShell.Helpers;
using UnityEngine;

namespace GameShell.Dialogs
{
    public class ShopDialog : Dialog
    {
        private ShopDialogHelper _helper;
        private PanelBuyInfo _panelBuyInfo;

        private Item _processItem;
       
        public ShopDialog(GameObject parent, GameObject view):base(parent, view, true)
        {
        }

        protected override void OnInit()
        {
            base.OnInit();

            _helper = View.GetComponent<ShopDialogHelper>();

            _panelBuyInfo = new PanelBuyInfo(_helper.PanelBuyInfo, _helper.PanelConnecting, _helper.PanelNotInternet, _helper.PanelEnjoy);
            _panelBuyInfo.Init();


            _processItem = _helper.Items[0];
            //foreach (var item in _helper.Items)
            //{
            //    var id = item;
            //    item.Button.onClick.AddListener(() => ClickBuy(id));
            //}
            _panelBuyInfo.Show(_processItem);
        }

        //private void OnLockButtonClick()
        //{
        //    foreach (var item in _helper.Items)
        //    {
        //        var id = item;
        //        ClickBuy(id);
        //        break;
        //    }
        //}

        //private void ClickBuy(Item id)
        //{
        //    _processItem = id;
        //    GS.Instance.ShowParentControlDialog(SuccesParenControl);
        //}

        private void SuccesParenControl()
        {
            _panelBuyInfo.Show(_processItem);
        }


        public override void Show()
        {
            base.Show();
        }

        public override void Hide()
        {
            base.Hide();
         }

        protected override void OnDestroy()
        {
            GS.Instance.EventSystem.Call(GameShellEvents.HideShopDialog);
            base.OnDestroy();
          }

        protected override void OnUpdate()
        {
        }
    }
}