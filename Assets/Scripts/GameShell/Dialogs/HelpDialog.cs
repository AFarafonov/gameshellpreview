﻿using UnityEngine;

namespace GameShell.Dialogs
{
    public class HelpDialog : Dialog
    {
        public HelpDialog(GameObject parent, GameObject view):base(parent, view)
        {
        }

        protected override void OnInit()
        {
            base.OnInit();

            //var settings = GS.Instance.Settings;
         
        }

        public override void Show()
        {
            base.Show();
            GS.Instance.EventSystem.Call(GameShellEvents.ShowMainMenu);
        }

        public override void Hide()
        {
            base.Hide();
            GS.Instance.EventSystem.Call(GameShellEvents.HideMainMenu);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
        }

        protected override void OnUpdate()
        {
        }
    }
}