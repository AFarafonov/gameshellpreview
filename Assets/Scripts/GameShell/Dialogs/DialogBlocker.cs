﻿using DG.Tweening;
using GameShell.Core.Extensions;
using UnityEngine;
using UnityEngine.UI;

namespace GameShell.Dialogs
{
    public class DialogBlocker
    {
        private readonly GameObject _blocker;
        private readonly Image _image;

        public DialogBlocker(GameObject parent, GameObject view)
        {
            _blocker = Object.Instantiate(view);
            _blocker.name = "Blocker";
            parent.AddChildUI(_blocker);
            _blocker.transform.SetSiblingIndex(0);
            _image = _blocker.GetComponent<Image>();

           // _image.DOFade(0f, 0f);
            _blocker.SetActive(false);
        }

        public void Show()
        {
            _blocker.SetActive(true);
         //   _image.DOFade(0.3f, 0.2f);
        }
       
        public void Hide()
        {
            //    _image.DOFade(0f, 0.2f).OnComplete(CompleteFade);
            CompleteFade();
        }

        private void CompleteFade()
        {
            _blocker.SetActive(false);
        }
    }
}