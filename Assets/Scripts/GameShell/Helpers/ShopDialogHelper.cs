﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GameShell.Helpers
{
    [Serializable]
    public struct Item
    {
        public string IdItem;
        public string DescriptionId;
        public string HeaderId;
        public Button Button;

        public Item(string id, string descriptionId, string headerId, Button button)
        {
            IdItem = id;
            Button = button;
            DescriptionId = descriptionId;
            HeaderId = headerId;
        }
    }

    public class ShopDialogHelper : MonoBehaviour
    {
        public GameObject PanelBuyInfo;
        public GameObject PanelConnecting;
        public GameObject PanelNotInternet;
        public GameObject PanelEnjoy;

        public List<Item> Items;
    }
}