﻿using UnityEngine;
using UnityEngine.UI;

namespace GameShell.Helpers
{
   public class AdAppIconHelper : MonoBehaviour
    {
        public Button Button;
        public Text Name;
        public Image Image;
        public Image ImageAccent;
    }
}