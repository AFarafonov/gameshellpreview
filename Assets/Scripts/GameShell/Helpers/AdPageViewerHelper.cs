﻿using UnityEngine;

namespace GameShell.Helpers
{
   public class AdPageViewerHelper : MonoBehaviour
    {
        public GameObject Content;
        public GameObject AnchorBtnClose;
        public GameObject Loader;
    }
}