﻿using UnityEngine;
using UnityEngine.UI;

namespace GameShell.Helpers
{
   public class BuyInfoPanelHelper : MonoBehaviour
    {
        public Button BuyButton;
        public Button RestoreButton;
        public Button LaterButton;
        public Text Header;
        public Text Description;
    }
}