﻿using System;
using System.Collections;
using SimpleJSON;
using UnityEngine;
using Object = UnityEngine.Object;

namespace GameShell.Server
{
    public class ItemAppData : ReceivedData
    {
        public int Id;
        public string Title;
        public string ImgUrl;
        public string Url;
        public Sprite SpriteImage;
        public string Accent = "";
        private Action _completeLoad;

        public ItemAppData(JSONNode data):base(data)
        {
        }

        public override void ParseAndLoad(Action complete)
        {
            _completeLoad = complete;

            Id = Data[Keys.Id].AsInt;
            Title = Data[Keys.Title];
            ImgUrl = Data[Keys.Img];
            Accent = Data[Keys.Accent];
            Url = Data[Keys.Url];

            GS.Instance.StartCoroutine(LoadImage(_completeLoad, ImgUrl));
        }

        private IEnumerator LoadImage(Action complete, string url)
        {
            var webIcon = new WWW(url);

            yield return webIcon;

            var txt = webIcon.texture;
            SpriteImage = Sprite.Create(webIcon.texture, new Rect(0, 0, txt.width, txt.height), new Vector2(0.5f, 0.5f));

            webIcon.Dispose();
            Object.Destroy(txt);

            complete();
        }

        public override void Destroy()
        {
            SpriteImage = null;
            _completeLoad = null;
            GS.Instance.StopCoroutine(LoadImage(_completeLoad, ImgUrl));
            base.Destroy();
        }
    }
}