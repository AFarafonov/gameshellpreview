﻿using System;
using SimpleJSON;

namespace GameShell.Server
{
    public class ReceivedData
    {
        protected JSONNode Data;
        protected int LoadedCounter = 0;

        public bool Loaded { get; protected set; }

        public ReceivedData(JSONNode data)
        {
            Data = data;
        }

        public virtual void ParseAndLoad(Action complete)
        {

        }

        public virtual void Destroy()
        {
            Data = null;
        }
    }
}
