﻿using System.Collections;
using System.Collections.Generic;
using GameShell.Core.EventSystem;
using SimpleJSON;
using UnityEngine;

namespace GameShell.Server
{
    public class AdService
    {
        public List<ItemAppData> BigAnnounces = new List<ItemAppData>();
        public List<SectionData> Sections = new List<SectionData>();
        public IconAdScreenData IconData;

        private int _loadCounter;
        private string _currentLangauge;
        private readonly string _url;
        private readonly SaveManager _saveManager;

        public AdService(SaveManager saveManager)
        {
            _saveManager = saveManager;
            _loadCounter = 0;
            _currentLangauge = GS.Instance.Localization.CurrentLocal;
            _url = GS.Instance.Settings.AdURL;

            GS.Instance.StartCoroutine(CheckLoad());
            GS.Instance.EventSystem.Attach(GameShellEvents.HideSettingsDialog, CheckChangeLangauge);
        }

        private void CheckChangeLangauge(EventParams eParams)
        {
            if (_currentLangauge != GS.Instance.Localization.CurrentLocal)
            {
                _currentLangauge = GS.Instance.Localization.CurrentLocal;

                _saveManager.AddSave(Keys.SaveAdLocalKey, _currentLangauge);

                Reload();
            }
        }

        public void Reload()
        {
            _saveManager.RemoveSaveItem(Keys.SaveAdLastUpdateKey);
            _saveManager.RemoveSaveItem(Keys.SaveAdDataKey);

            GS.Instance.EventSystem.Call(GameShellEvents.ReloadAnnounce);
            Destroy();

            GS.Instance.StartCoroutine(CheckLoad());
        }

        public void Destroy()
        {
            IconData.Destroy();
            IconData = null;

            foreach (ItemAppData bigAnnounce in BigAnnounces)
            {
                bigAnnounce.Destroy();
            }

            foreach (SectionData sectionData in Sections)
            {
                sectionData.Destroy();
            }

            BigAnnounces.Clear();
            Sections.Clear();
        }

        private IEnumerator CheckLoad()
        {
            var lastUpdate = _saveManager.GetSavedInt(Keys.SaveAdLastUpdateKey);
			var url = _url + "?action=anns&lang=" + _currentLangauge + "&platform=" + GetPlatformName() + "&updated_at=" + lastUpdate + "&app=" + WWW.EscapeURL(Application.productName);

            var webClient = new WWW(url);
            yield return webClient;

            if (webClient.error == null)
            {
                var data = JSON.Parse(webClient.text);
                var status = data[Keys.Status].Value;
              //  Debug.Log(status);
                if (status == Keys.SuccesStatus)
                {
                    _saveManager.AddSave(Keys.SaveAdLastUpdateKey, data[Keys.UpdatedAt].Value);
                    _saveManager.AddSave(Keys.SaveAdLocalKey, GS.Instance.Localization.CurrentLocal);
                    _saveManager.AddSave(Keys.SaveAdDataKey, data);
                    _saveManager.Save();
                }
                else if (status == Keys.UptodateStatus)
                {
                    data = _saveManager.GetSavedNode(Keys.SaveAdDataKey);
                }

                if (status != Keys.Error)
                {
                    ParseData(data);
                }
            }

            webClient.Dispose();
        }

        private void ParseData(JSONNode data)
        {
            if (data != null)
            {
                IconData = new IconAdScreenData(data);
                IconData.ParseAndLoad(CompleteImageLoad);
                _loadCounter++;

                var biggAnns = data[Keys.BigAnnons].AsArray;
                foreach (var itemBig in biggAnns.Childs)
                {
                    var bigAnnons = new ItemAppData(itemBig);
                    bigAnnons.ParseAndLoad(CompleteImageLoad);
                    BigAnnounces.Add(bigAnnons);
                    _loadCounter++;
                }

                var sections = data[Keys.Sections].AsArray;
                foreach (var dataSection in sections.Childs)
                {
                    var section = new SectionData(dataSection);
                    section.ParseAndLoad(CompleteImageLoad);
                    Sections.Add(section);
                    _loadCounter++;
                }
            }
        }

        private void CompleteImageLoad()
        {
            _loadCounter--;

            if (_loadCounter == 0)
            {
                GS.Instance.EventSystem.Call(GameShellEvents.CompleteAnnounceLoad);
            }
        }

        private string GetPlatformName()
        {
            string platformName = "";
            switch (Application.platform)
            {
                case RuntimePlatform.Android:
                    platformName = "android";
                    break;

                case RuntimePlatform.IPhonePlayer:
                    platformName = "ios";
                    break;
                case RuntimePlatform.WP8Player:
                    platformName = "wp";
                    break;
                case RuntimePlatform.WindowsEditor:
                    platformName = "android";
                    break;
            }
            return platformName;
        }
    }
}