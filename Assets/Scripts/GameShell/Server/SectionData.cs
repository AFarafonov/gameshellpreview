﻿using System;
using System.Collections.Generic;
using SimpleJSON;

namespace GameShell.Server
{
    public class SectionData : ReceivedData
    {
        public string Title;
        public List<ItemAppData> AppItems = new List<ItemAppData>();

        private Action _completeAction;
      
        public SectionData(JSONNode data):base(data)
        {
        }

        public override void ParseAndLoad(Action complete)
        {
            LoadedCounter = 0;
            _completeAction = complete;

            Title = Data[Keys.Title];

            var items = Data[Keys.Items].AsArray;
            foreach (var dataApp in items.Childs)
            {
                var section = new ItemAppData(dataApp);
                section.ParseAndLoad(CompleteLoad);
                AppItems.Add(section);
                LoadedCounter++;
            }
        }

        private void CompleteLoad()
        {
            LoadedCounter--;

            if (LoadedCounter == 0 && _completeAction != null)
            {
                _completeAction();
            }
        }

        public override void Destroy()
        {
            foreach (ItemAppData appItem in AppItems)
            {
                appItem.Destroy();
            }
            AppItems.Clear();
            _completeAction = null;

            base.Destroy();
        }
    }
}
