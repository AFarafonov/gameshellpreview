﻿using System;
using System.Collections;
using SimpleJSON;
using UnityEngine;
using Object = UnityEngine.Object;

namespace GameShell.Server
{
    public class IconAdScreenData : ReceivedData
    {
        public string IconText;
        public string ImgUrl;
        public Sprite SpriteImage;
        public Color Color;
        private Action _completeLoad;

        public IconAdScreenData(JSONNode data):base(data)
        {
        }

        public override void ParseAndLoad(Action complete)
        {
            _completeLoad = complete;
            
            IconText = Data["icon_text"].Value;
            ImgUrl = Data["icon"].Value;
            var collorArray = Data["icon_bg_color"].AsArray;

            if (collorArray != null)
            {
                Color = new Color(collorArray[0].AsFloat / 255f, collorArray[1].AsFloat / 255f, collorArray[2].AsFloat / 255f);               
            }

            GS.Instance.StartCoroutine(LoadImage(_completeLoad, ImgUrl));
        }

        private IEnumerator LoadImage(Action complete, string url)
        {
            var webIcon = new WWW(url);

            yield return webIcon;

            var txt = webIcon.texture;
            SpriteImage = Sprite.Create(webIcon.texture, new Rect(0, 0, txt.width, txt.height), new Vector2(0.5f, 0.5f));

            webIcon.Dispose();
            Object.Destroy(txt);

            complete();
        }

        public override void Destroy()
        {
            SpriteImage = null;
            _completeLoad = null;
            GS.Instance.StopCoroutine(LoadImage(_completeLoad, ImgUrl));

            base.Destroy();
        }
    }
}