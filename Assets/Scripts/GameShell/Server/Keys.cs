﻿
namespace GameShell.Server
{
    public class Keys
    {
        public const string None = "None";
        public const string Id = "id";
        public const string Img = "img";
        public const string Video = "video";
        public const string Paragraph = "p";
        public const string HeaderBig = "h1";
        public const string HeaderSmall = "h2";
        public const string Blockquote = "blockquote";
        public const string Title = "title";
        public const string Page = "page";
        public const string Url = "url";
        public const string Accent = "accent";
        public const string Sections = "sections";
        public const string Items = "items";
        public const string BigAnnons = "bigg_anns";
        public const string SaveAdDataKey = "ad_data";
        public const string SaveAdLocalKey = "ad_local";
        public const string SaveAdLastUpdateKey = "ad_last_update";
        public const string Status = "status";
        public const string SuccesStatus = "success";
        public const string UptodateStatus = "uptodate";
        public const string UpdatedAt = "updated_at";
        public const string Error = "error";
    }
}