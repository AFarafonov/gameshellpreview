﻿using System;
using System.Collections.Generic;
using GameShell.Buttons;
using GameShell.Core;
using GameShell.Core.EventSystem;
using GameShell.Dialogs;
using GameShell.Screens;
using GameShell.Screens.AdScreen;
using UnityEngine;
using DG.Tweening;
using GameShell.Server;

namespace GameShell
{
    public enum Scene
    {
        None,
        Preload,
        MainMenu,
        Ad,
        Game
    }

    public class GS : MonoBehaviour {
        
        public EventSystem EventSystem { get; private set; }
        public LocalizationSystem Localization { get; private set; }
        public Dictionary<string, Sprite> AccentSprites = new Dictionary<string, Sprite>();

        [HideInInspector]public GameObject RootUgui { get; private set; }
        [HideInInspector]public GameObject DialogsContainer { get; private set; }
        [HideInInspector]public GameObject ScreensContainer { get; private set; }
        [HideInInspector]public GameObject GameContainer { get; private set; }

        public DialogBlocker Blocker { get; private set; }
        public SettingsGameShell Settings { get; private set; }
        public float VolumeMusic { get; set; }
        public float VolumeSound { get; set; }

        public Action CallbackUpdate;
        public AdService Service;
        public SaveManager SaveManager;
        public InAppPurchaseManager InAppPurchaseManager;

        public static GS Instance {
            get { return _instance ?? (_instance = new GS()); }
        }

        private SplashScreen _splashScreen;
        private MainMenuScreen _mainMenuScreen;
        private AdScreen _adScreen;
        private ControlPanel _controlPanel;
        private static GS _instance;

        public Scene CurrentScene
        {
            get { return StateMachine.CurrentState; }
        }

        protected readonly StateMachine<Scene> StateMachine = new StateMachine<Scene>();

        private GS()
        {
            _instance = this;
        }

        void Start ()
        {
            if (Instance != null)
            {
                VolumeMusic = 1f;
                VolumeSound = 1f;

                DOTween.Init(false, false, LogBehaviour.ErrorsOnly);

                Settings = Resources.Load<SettingsGameShell>("GameShell/Data/SettingsGameShell");

                RootUgui = transform.FindChild("UGUI").gameObject;// GameObject.Find("UGUI");

                if (RootUgui == null)
                {
                    throw new Exception("Not found UGUI in GS gameObject");
                }

                ScreensContainer = RootUgui.transform.FindChild("Screens").gameObject;
                DialogsContainer = RootUgui.transform.FindChild("Dialogs").gameObject;
                GameContainer = RootUgui.transform.FindChild("GameContainer").gameObject;

                EventSystem = new EventSystem();
                Localization = new LocalizationSystem(Settings.PathToLocalizationFile, EventSystem, Settings.EnableLocalization);
                Blocker = new DialogBlocker(DialogsContainer, Settings.BlockerDialog);

                foreach (AccentItem accentItem in Settings.AccentItems)
                {
                    AccentSprites.Add(accentItem.Id, accentItem.Sprite);
                }

                SaveManager = new SaveManager();
                Service = new AdService(SaveManager);
                InAppPurchaseManager = new InAppPurchaseManager();

                Init();
            }
        }

        private void Init()
        {
            StateMachine.Add(Scene.Preload, OnPreload);
            StateMachine.Add(Scene.MainMenu, OnMainMenu, OnHideMainMenu);
            StateMachine.Add(Scene.Ad, OnAdScreen, OnHideAdScreen);
            StateMachine.Add(Scene.Game, OnGame, OnHideGame);
            StateMachine.SetState(Scene.None);

            _splashScreen = new SplashScreen(ScreensContainer, Settings.SplashScreen, Settings.TimeSplashComplete);
            
            _mainMenuScreen = new MainMenuScreen(ScreensContainer, Settings.MainMenu, ClickPlay, ClickAd);
            _mainMenuScreen.Init();
            _mainMenuScreen.Hide();

            _adScreen = new AdScreen(Service, ScreensContainer, Settings.AdScreen, () => { StateMachine.SetState(Scene.MainMenu); });
            _adScreen.Init();
            _adScreen.Hide();
            
            _controlPanel = new ControlPanel(ScreensContainer, Settings.ControlPanel, () => { StateMachine.SetState(Scene.MainMenu); });
            _controlPanel.Init();
            _controlPanel.Hide();

            EventSystem.Call(GameShellEvents.GameShellInit);
            StateMachine.SetState(Scene.Preload);
        }

        private void ClickPlay()
        {
            EventSystem.Call(GameShellEvents.ClickPlayGame);
            StateMachine.SetState(Scene.Game);
        }

        private void ClickAd()
        {
            StateMachine.SetState(Scene.Ad);
        }

        private void OnPreload(object obj)
        {
            _splashScreen.Start(CompletePreload);
            EventSystem.Call(GameShellEvents.StartShowSplash);
        }

        private void CompletePreload()
        {
            _splashScreen.Destroy();
            _splashScreen = null;

            EventSystem.Call(GameShellEvents.CompleteShowSplash);

            StateMachine.SetState(Scene.MainMenu);
        }

        private void OnMainMenu(object obj)
        {
            _mainMenuScreen.Show();
        }

        private void OnHideMainMenu()
        {
            _mainMenuScreen.Hide();
        }

        private void OnAdScreen(object obj)
        {
            _adScreen.Show();
        }

        private void OnHideAdScreen()
        {
            _adScreen.Hide();
        }

        private void OnGame(object obj)
        {
            _controlPanel.Show();
        }

        private void OnHideGame()
        {
            _controlPanel.Hide();
        }

        public void Destroy()
        {
            EventSystem.Call(GameShellEvents.GameShellDestroy);
        }

        public void ShowMenuScreen()
        {
            StateMachine.SetState(Scene.MainMenu);
        }

        public void ShowGameControlPanel()
        {
            StateMachine.SetState(Scene.Game);
        }

        public void ShowSettingsDialog()
        {
            var settingsDialog = new SettingsDialog(DialogsContainer, Settings.SettingsDialog);
            settingsDialog.Init();
        }

        public void ShowParentControlDialog(Action succesAction)
        {
            var parentControlDialog = new ParentControlDialog(DialogsContainer, Settings.ParentControlDialog, succesAction);
            parentControlDialog.Init();
        }

        public void ShowBackArrowButton()
        {
            EventSystem.Call(GameShellEvents.ShowArrowButton, new ArrowButtonParams(ArrowButtonType.Back));
        }

        public void ShowForwardArrowButton()
        {
            EventSystem.Call(GameShellEvents.ShowArrowButton, new ArrowButtonParams(ArrowButtonType.Forward));
        }

        public void HideBackArrowButton()
        {
            EventSystem.Call(GameShellEvents.HideArrowButton, new ArrowButtonParams(ArrowButtonType.Back));
        }

        public void HideForwardArrowButton()
        {
            EventSystem.Call(GameShellEvents.HideArrowButton, new ArrowButtonParams(ArrowButtonType.Forward));
        }

        private void Update ()
        {
            if(_splashScreen != null)
                _splashScreen.Update();

            if (CallbackUpdate != null)
                CallbackUpdate();
        }


    }
}