﻿using System.Collections.Generic;
using GameShell.Helpers;
using GameShell.Server;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace GameShell.Editor
{
    [CustomEditor(typeof(ShopDialogHelper))]
    [CanEditMultipleObjects]
    public class ShopDialogHelperEditor : UnityEditor.Editor
    {
        private ShopDialogHelper _helper;

        void OnEnable()
        {
        }

        public override void OnInspectorGUI()
        {
            _helper = this.target as ShopDialogHelper;
            if (_helper == null) return;

            serializedObject.Update();

            _helper.PanelBuyInfo = (GameObject)EditorGUILayout.ObjectField("Info:", _helper.PanelBuyInfo, typeof(GameObject), true);
            _helper.PanelConnecting = (GameObject)EditorGUILayout.ObjectField("Connecting:", _helper.PanelConnecting, typeof(GameObject), true);
            _helper.PanelNotInternet = (GameObject)EditorGUILayout.ObjectField("NotInternet:", _helper.PanelNotInternet, typeof(GameObject), true);
            _helper.PanelEnjoy = (GameObject)EditorGUILayout.ObjectField("Enjoy:", _helper.PanelEnjoy, typeof(GameObject), true);
            /*
            _helper.HeaderText = (Text)EditorGUILayout.ObjectField("Header:", _helper.HeaderText, typeof(Text), true);
            _helper.DescriptionText = (Text)EditorGUILayout.ObjectField("Description:", _helper.DescriptionText, typeof(Text), true);
            _helper.TargetNumberText = (Text)EditorGUILayout.ObjectField("Target Number:", _helper.TargetNumberText, typeof(Text), true);
            _helper.InputText = (Text)EditorGUILayout.ObjectField("Input:", _helper.InputText, typeof(Text), true);
            */
            EditorGUILayout.Space();

            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Add"))
                AddElement();

            if (GUILayout.Button("ClearAll"))
                ClearAll();

            GUILayout.EndHorizontal();

            EditorGUILayout.Space();
            
            var removeList = new List<Item>();
            for (var j = 0; j < _helper.Items.Count; j++)
            {
                var associationsItem = _helper.Items[j];
                
                GUILayout.BeginHorizontal();
                var togle = (associationsItem.Button != null) ? associationsItem.Button.name : "null";

                EditorGUILayout.LabelField("   Buy Item: [" + associationsItem.IdItem + "]  Button (" + togle + ")");

                if (GUILayout.Button("X", GUI.skin.label, new GUILayoutOption[]
                        {
                            GUILayout.Width(16),
                            GUILayout.Height(16)
                        }))
                {
                    removeList.Add(associationsItem);
                }
                GUILayout.EndHorizontal();
                
                GUILayout.BeginHorizontal();
                GUILayout.Label("", GUILayout.Width(21));
                associationsItem.IdItem = EditorGUILayout.TextField("Id item:", associationsItem.IdItem);
                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();
                GUILayout.Label("", GUILayout.Width(21));
                associationsItem.DescriptionId = EditorGUILayout.TextField("Id description:", associationsItem.DescriptionId);
                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();
                GUILayout.Label("", GUILayout.Width(21));
                associationsItem.HeaderId = EditorGUILayout.TextField("Id header:", associationsItem.HeaderId);
                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();
                GUILayout.Label("", GUILayout.Width(21));

                associationsItem.Button = (Button)EditorGUILayout.ObjectField("Button:", associationsItem.Button, typeof(Button), true);
                GUILayout.EndHorizontal();

                _helper.Items[j] = associationsItem;
            }

            foreach (var item in removeList)
            {
                _helper.Items.Remove(item);
            }

            removeList.Clear();

            serializedObject.ApplyModifiedProperties();
        }



        private void AddElement()
        {
            _helper.Items.Add(new Item(Keys.None, Keys.None, Keys.None, null));
        }

        private void ClearAll()
        {
            _helper.Items.Clear();
        }
    }
}