﻿using System;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace GameShell.Editor
{
    public class Menu
    {
        [MenuItem("GS/Add GS Container in Scene", false, 600)]
        public static void AddGameShellContainer()
        {
            var source = AssetDatabase.LoadAssetAtPath("Assets/Resources/GameShell/GameShell.prefab", typeof(GameObject));
            if (source == null)
                throw new Exception("Not found GameShell.prefab in directory Assets/Resources/GameShell/");

            GameObject go = PrefabUtility.InstantiatePrefab(source) as GameObject;
            go.name = "GameShell";
            Selection.activeObject = go;
            Debug.Log("Succes add GS container in current scene");
        }

        [MenuItem("GS/Create new Settings", false, 601)]
        public static void CreateSettings()
        {
            AssetDatabase.DeleteAsset("Assets/Resources/GameShell/Data/SettingsGameShell.asset");
            AssetDatabase.Refresh();

            CreateAsset<SettingsGameShell>("GameShell/Data/");          

            Debug.Log("Succes created Settings.asset");
        }

        [MenuItem("GS/Show Settings")]
        public static void ShowSettings()
        {
            var asset = Resources.Load<SettingsGameShell>("GameShell/Data/SettingsGameShell");
            EditorUtility.FocusProjectWindow();
            Selection.activeObject = asset;
        }      

        public static void CreateAsset<T>(string path) where T : ScriptableObject
        {
            var asset = ScriptableObject.CreateInstance<T>();
        
            var savePath = Path.Combine("Assets/Resources", path);
            var fullPath = Path.Combine(Application.dataPath + "/Resources", path);
        
            if (Directory.Exists(fullPath) == false)
            {
                Directory.CreateDirectory(fullPath);
            }

            string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(savePath + "/" + typeof(T).Name + ".asset");

            AssetDatabase.CreateAsset(asset, assetPathAndName);

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();

            EditorUtility.FocusProjectWindow();
            Selection.activeObject = asset;
        }
    }
}
