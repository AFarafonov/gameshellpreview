﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using GameShell.Server;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace GameShell.Editor
{// [CanEditMultipleObjects]
    [CustomEditor(typeof(SettingsGameShell))]
    public class SettingsGameShellEditor : UnityEditor.Editor
    {
        private SettingsGameShell _settings;

        void OnEnable()
        {
        }

        public override void OnInspectorGUI()
        {
            _settings = target as SettingsGameShell;
            if (_settings == null) return;

           // serializedObject.UpdateIfDirtyOrScript();
           
            GUILayout.Box("CONFIGURATION", GUILayout.MaxWidth(Screen.width));
            Separator();

            GUILayout.BeginVertical("LOCALIZATION", "box");
            GUILayout.Space(20);

            _settings.EnableLocalization = GUILayout.Toggle(_settings.EnableLocalization, " enable");

            if (_settings.EnableLocalization)
            {
                var path = (string.IsNullOrEmpty(_settings.PathToLocalizationFile)) ? "none": _settings.PathToLocalizationFile;
                EditorGUILayout.LabelField("Path csv file:", path);

                if (GUILayout.Button("Set path"))
                {
                    var choosenPath = EditorUtility.OpenFilePanel("Select localization CSV file", Application.dataPath, "csv");
                    if (choosenPath.Length == 0) return;
                     choosenPath = Path.ChangeExtension(choosenPath, "");
                   
                    var resultPath = choosenPath.Split('/').ToList();

                    bool enable = false;
                    string result = "";
                    foreach (string dir in resultPath)
                    {
                        if (enable)
                            result += dir + "/";

                        if (dir == "Resources")
                            enable = true;
                    }
                    result = result.Remove(result.IndexOf('.'), 2);
                    _settings.PathToLocalizationFile = result;
                }
            }

            GUILayout.Space(10);
            GUILayout.EndVertical();

            GUILayout.BeginVertical("SPLASH SCREEN", "box");
            GUILayout.Space(20);

            _settings.SplashScreen = (GameObject)EditorGUILayout.ObjectField("Splash prefab:", _settings.SplashScreen, typeof(GameObject), true);
            _settings.TimeSplashComplete = EditorGUILayout.FloatField("Time complete:", _settings.TimeSplashComplete);
            _settings.AnimationSplashComplete = GUILayout.Toggle(_settings.AnimationSplashComplete, " animation complete");

            GUILayout.Space(10);
            GUILayout.EndVertical();

            GUILayout.BeginVertical("MAIN MENU SCREEN", "box");
            GUILayout.Space(20);

            _settings.MainMenu = (GameObject)EditorGUILayout.ObjectField("Screen:", _settings.MainMenu, typeof(GameObject), true);
            _settings.BackgroundMenu = (GameObject)EditorGUILayout.ObjectField("Background:", _settings.BackgroundMenu, typeof(GameObject), true);

            GUILayout.Space(10);
            EditorGUILayout.LabelField("BUTTONS");
            Separator();

            _settings.ButtonPlay = (GameObject)EditorGUILayout.ObjectField("Play:", _settings.ButtonPlay, typeof(GameObject), true);
            _settings.ButtonSettings = (GameObject)EditorGUILayout.ObjectField("_settings:", _settings.ButtonSettings, typeof(GameObject), true);
            _settings.ButtonAd = (GameObject)EditorGUILayout.ObjectField("Ad:", _settings.ButtonAd, typeof(GameObject), true);
            _settings.ButtonHelp = (GameObject)EditorGUILayout.ObjectField("Help:", _settings.ButtonHelp, typeof(GameObject), true);

            GUILayout.Space(10);
            GUILayout.EndVertical();

            GUILayout.BeginVertical("CONTROL GAME SCREEN", "box");
            GUILayout.Space(20);

            _settings.ControlPanel = (GameObject)EditorGUILayout.ObjectField("Control panel:", _settings.ControlPanel, typeof(GameObject), true);
            _settings.EnableArrowButtons = GUILayout.Toggle(_settings.EnableArrowButtons, " enable arrow buttons");

            GUILayout.Space(10);
            EditorGUILayout.LabelField("BUTTONS");
            Separator();

            _settings.ButtonForward = (GameObject)EditorGUILayout.ObjectField("Formard:", _settings.ButtonForward, typeof(GameObject), true);
            _settings.ButtonBack = (GameObject)EditorGUILayout.ObjectField("Back:", _settings.ButtonBack, typeof(GameObject), true);
          
            GUILayout.Space(10);
            GUILayout.EndVertical();

            GUILayout.BeginVertical("COMMON BUTTONS", "box");
            GUILayout.Space(20);
            _settings.TimeDeactivationButtons = EditorGUILayout.FloatField("Time deactivation:", _settings.TimeDeactivationButtons);
            _settings.ButtonHome = (GameObject)EditorGUILayout.ObjectField("Home:", _settings.ButtonHome, typeof(GameObject), true);
            _settings.ButtonLock = (GameObject)EditorGUILayout.ObjectField("Lock:", _settings.ButtonLock, typeof(GameObject), true);
            _settings.ButtonClose = (GameObject)EditorGUILayout.ObjectField("Close:", _settings.ButtonClose, typeof(GameObject), true);
            _settings.ButtonReturn = (GameObject)EditorGUILayout.ObjectField("Return:", _settings.ButtonReturn, typeof(GameObject), true);

            GUILayout.Space(10);
            GUILayout.EndVertical();

            GUILayout.BeginVertical("COMMON DIALOGS", "box");
            GUILayout.Space(20);

            _settings.ShopDialog = (GameObject)EditorGUILayout.ObjectField("Shop:", _settings.ShopDialog, typeof(GameObject), true);
            _settings.SettingsDialog = (GameObject)EditorGUILayout.ObjectField("Settings:", _settings.SettingsDialog, typeof(GameObject), true);
            _settings.HelpDialog = (GameObject)EditorGUILayout.ObjectField("Help:", _settings.HelpDialog, typeof(GameObject), true);
            _settings.ParentControlDialog = (GameObject)EditorGUILayout.ObjectField("ParentControl:", _settings.ParentControlDialog, typeof(GameObject), true);
            _settings.BlockerDialog = (GameObject)EditorGUILayout.ObjectField("Blocker:", _settings.BlockerDialog, typeof(GameObject), true);
            _settings.LockScreen = (GameObject)EditorGUILayout.ObjectField("LockScreen:", _settings.LockScreen, typeof(GameObject), true);

            GUILayout.Space(10);
            GUILayout.EndVertical();

            GUILayout.BeginVertical("AD SCREEN", "box");
            GUILayout.Space(20);

            _settings.AdURL = EditorGUILayout.TextField("URL:", _settings.AdURL);
            _settings.AdScreen = (GameObject)EditorGUILayout.ObjectField("Screen:", _settings.AdScreen, typeof(GameObject), true);          
            _settings.AdHeaderPreviewIcon = (GameObject)EditorGUILayout.ObjectField("Header preview icon:", _settings.AdHeaderPreviewIcon, typeof(GameObject), true);          
            _settings.AdSection = (GameObject)EditorGUILayout.ObjectField("Section:", _settings.AdSection, typeof(GameObject), true);          
            _settings.AdIconSection = (GameObject)EditorGUILayout.ObjectField("Section icon:", _settings.AdIconSection, typeof(GameObject), true);   

            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Add AssociationItem"))
                AddElement();

            if (GUILayout.Button("ClearAll"))
                ClearAll();

            GUILayout.EndHorizontal();

            EditorGUILayout.Space();
            
            var removeList = new List<AccentItem>();
            for (var i = 0; i < _settings.AccentItems.Count; i++)
            {
                var associationsItem = _settings.AccentItems[i];

                GUILayout.Space(10);
                GUILayout.BeginVertical();
                    GUILayout.BeginHorizontal();
                    GUILayout.Label("Element " + i);
                    if (GUILayout.Button("X", GUI.skin.label, new GUILayoutOption[]
                          {
                                GUILayout.Width(16),
                                GUILayout.Height(16)
                          }))
                    {
                        removeList.Add(associationsItem);
                    }

                    GUILayout.EndHorizontal();

                associationsItem.Id = EditorGUILayout.TextField("Id:", associationsItem.Id);
                associationsItem.Sprite = (Sprite)EditorGUILayout.ObjectField("Sprite:", associationsItem.Sprite, typeof(Sprite), true);
               
                GUILayout.EndVertical();
                _settings.AccentItems[i] = associationsItem;
            }

            foreach (var item in removeList)
            {
                _settings.AccentItems.Remove(item);
            }

            removeList.Clear();

            GUILayout.Space(10);
            GUILayout.EndVertical();

            //DrawDefaultInspector();
            if (GUI.changed)
            {
                EditorUtility.SetDirty(_settings);
            }
        }

        private void AddElement()
        {
            _settings.AccentItems.Add(new AccentItem(Keys.None, null));
        }

        private void ClearAll()
        {
            _settings.AccentItems.Clear();
        }

        public void Separator()
        {
            GUI.backgroundColor = Color.black;
            GUILayout.Box("", GUILayout.MaxWidth(Screen.width), GUILayout.Height(2));
            GUI.backgroundColor = Color.white;
        }

    }
}