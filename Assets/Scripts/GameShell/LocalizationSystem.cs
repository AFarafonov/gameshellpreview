﻿using System;
using System.Collections.Generic;
using GameShell.Core.EventSystem;
using UnityEngine;

namespace GameShell
{
    public class LocalizationSystem
    {
        private readonly EventSystem _eventSystem;
        public string CurrentLocal;

        private Dictionary<string, Dictionary<string, string>> _local;
        private Dictionary<string, string> _currentLocalDictionary;

        public LocalizationSystem(string pathCvs, EventSystem eventSystem, bool enable)
        {
            _eventSystem = eventSystem;

            if (enable)
            {
                TextAsset text = Resources.Load(pathCvs) as TextAsset;

                if (text != null)
                    ParseCsvFile(text.text);
                else
                    throw new Exception("Not load csv file!");

                SetLangauge(Application.systemLanguage.ToString());
            }
            else
            {
                CurrentLocal = Application.systemLanguage.ToString();
            }
        }

        private void SetLangauge(string local)
        {
            if (_local == null)
            {
                CurrentLocal = local;
                return;
            }

#if UNITY_ANDROID || UNITY_IOS
            if (!_local.ContainsKey(local))
                local = "English";
#endif
#if UNITY_EDITOR
            if (!_local.ContainsKey(local))
                throw new Exception("Not found Local -> " + local);
#endif
            CurrentLocal = local;
            _currentLocalDictionary = _local[CurrentLocal];
        }

        public void ChangeLanguage(string local)
        {
            if(CurrentLocal == local)return;

            SetLangauge(local);

           _eventSystem.Call(GameShellEvents.ChangeLangauge, new LangaugeParams(local));
        }

        private void ParseCsvFile(string textAsset)
        {
            _local = new Dictionary<string, Dictionary<string, string>>();
            textAsset = textAsset.Replace('\n', '\r');
            var lines = textAsset.Split(new[] { '\r' }, StringSplitOptions.RemoveEmptyEntries);

            var numberLines = lines.Length;
            var numberRows = lines[0].Split(',').Length;

            var associationLocals = new Dictionary<int, string>(numberRows - 1);
         
            var  localId = lines[0].Split(',');
            for (var i = 1; i < numberRows; i++)
            {
                _local.Add(localId[i], new Dictionary<string, string>(numberLines - 1));
                associationLocals.Add(i, localId[i]);
            }

            for (var r = 1; r < numberLines; r++)
            {
                //var line = LineParser(lines[r], numberRows);
                var line = LineParser(lines[r]);
                var currentStringId = line[0];
                try
                {
                    foreach (var local in _local)
                    {
                        local.Value.Add(currentStringId, null);
                    }
                }
                catch (ArgumentException e)
                {
                    Debug.Log("Please remove duplicating keys in your localization table   " + currentStringId + "  " + e.Message);
                }  

                for (var c = 1; c < numberRows; c++)
                {
                    var currentValue = line[c];
                    if (string.IsNullOrEmpty(currentValue)) continue;
                 
                    var key = associationLocals[c];
                    var value = _local[key];
                    value[currentStringId] = currentValue;
                }
            }
        }

        private string[] LineParser(string line, int rows)
        {
            var result = new string[rows];
            var targetline = line.ToCharArray();

            string newLine = "";
            int count = 0;
            bool skipComma = false;
            for (var i = 0; i < targetline.Length; i++)
            {
                var symbol = targetline[i];
                if (symbol == ',')
                {
                    if(skipComma)
                    {
                        if (targetline[i - 1] == '"')
                            skipComma = false;
                        else
                            newLine += symbol;
                    }
                    else
                    {
                        result[count] = newLine;
                        count++;
                        newLine = "";

                        if (targetline.Length > i+1 && targetline[i + 1] == '"')
                            skipComma = true;
                    }
                }
                else
                {
                    if (symbol == '"')
                    {
                        if ((i - 1 > -1 && targetline[i - 1] == ',') || (targetline.Length > i + 1 && targetline[i + 1] == ',') || targetline.Length == i + 1)
                            symbol = '\0';
                    }

                    if(symbol != '\0')
                        newLine += symbol;

                    if (i == targetline.Length - 1)
                        result[count] = newLine;
                }              
            }

            for (var j = 0; j < result.Length; j++)
            {
                var str = result[j];
                if (str != null)
                {
                    str = str.Replace("\"\"", "\"");
                    str = str.Replace("\\" + "n", "\n");
                    str = str.Replace("\\" + "\n", "\\" + "n");
                    result[j] = str;
                }
            }

            return result;
        }

        private string[] LineParser(string r)
        {
            string[] c;
            List<string> resp = new List<string>();
            bool cont = false;
            string cs = "";

            c = r.Split(new char[] { ',' }, StringSplitOptions.None);

            foreach (string y in c)
            {
                string x = y;


                if (cont)
                {
                    // End of field
                    if (x.EndsWith("\""))
                    {
                        cs += "," + x.Substring(0, x.Length - 1);
                        cs = cs.Replace("\\n", "\n");
                        cs = cs.Replace("\"\"", "\"");
                        resp.Add(cs);
                        cs = "";
                        cont = false;
                        continue;

                    }
                    else
                    {
                        // Field still not ended
                        cs += "," + x;
                        continue;
                    }
                }

                // Fully encapsulated with no comma within
                if (x.StartsWith("\"") && x.EndsWith("\""))
                {
                    if ((x.EndsWith("\"\"") && !x.EndsWith("\"\"\"")) && x != "\"\"")
                    {
                        cont = true;
                        cs = x;
                        continue;
                    }

                    string res = x.Substring(1, x.Length - 2);
                    res = res.Replace("\\n", "\n");
                    res = res.Replace("\"\"", "\"");
                    resp.Add(res);
                    continue;
                }

                // Start of encapsulation but comma has split it into at least next field
                if (x.StartsWith("\"") && !x.EndsWith("\""))
                {
                    cont = true;
                    cs += x.Substring(1);
                    continue;
                }

                // Non encapsulated complete field
                x = x.Replace("\\n", "\n");
                x = x.Replace("\"\"", "\"");
                resp.Add(x);

            }

            return resp.ToArray();

        }

        public string GetString(string key)
        {
            if (!_currentLocalDictionary.ContainsKey(key))
            {
                throw new Exception("Error wrong key! " + key);
            }
            
            var value = _currentLocalDictionary[key] ?? "id: " + key;
            return value;
        }
    }
}