﻿using GameShell.Buttons;
using GameShell.Core.EventSystem;

namespace GameShell
{
    public class GameShellEvents
    {
        public static Event ClickPlayGame = new Event(WrapTag("ClickPlayGame"));
        public static Event StartShowSplash = new Event(WrapTag("StartShowSplash"));
        public static Event CompleteShowSplash = new Event(WrapTag("CompleteShowSplash"));

        public static Event ShowMainMenu = new Event(WrapTag("ShowMainMenu"));
        public static Event HideMainMenu = new Event(WrapTag("HideMainMenu"));

        public static Event ShowControlPanel = new Event(WrapTag("ShowControlPanel"));
        public static Event HideControlPanel = new Event(WrapTag("HideControlPanel"));

        public static Event ShowSettingsDialog = new Event(WrapTag("ShowSettingsDialog"));
        public static Event HideSettingsDialog = new Event(WrapTag("HideSettingsDialog"));

        public static Event ShowParentControlDialog = new Event(WrapTag("ShowParentControlDialog"));
        public static Event HideParentControlDialog = new Event(WrapTag("HideParentControlDialog"));

        public static Event GameShellInit = new Event(WrapTag("GameShellInit"));
        public static Event GameShellDestroy = new Event(WrapTag("GameShellDestroy"));

        public static Event HideArrowButton = new Event(WrapTag("HideArrowButton"));
        public static Event ShowArrowButton = new Event(WrapTag("ShowArrowButton"));
     
        public static Event ClickBackArrowButton = new Event(WrapTag("ClickBackArrowButton"));
        public static Event ClickForwardArrowButton = new Event(WrapTag("ClickForwardArrowButton"));

        public static Event ChangeLangauge = new Event(WrapTag("ChangeLangauge"));

        public static Event CompleteAnnounceLoad = new Event(WrapTag("CompleteAnnounceLoad"));
        public static Event ReloadAnnounce = new Event(WrapTag("ReloadAnnounce"));
        
        public static Event ChangeVolumeSound = new Event(WrapTag("ChangeVolumeSound"));
        public static Event ChangeVolumeMusic = new Event(WrapTag("ChangeVolumeMusic"));

        public static Event HideShopDialog = new Event(WrapTag("HideShopDialog"));
        public static Event ShowShopDialog = new Event(WrapTag("ShowShopDialog"));

        public static Event PurchaseCompleted = new Event(WrapTag("PurchaseCompleted"));
        public static Event CloseLockScreen = new Event(WrapTag("CloseLockScreen"));
        public static Event ClickLockButton = new Event(WrapTag("ClickLockButton"));

        public static Event ButtonCLick = new Event(WrapTag("ButtonCLick"));
        public static Event PanelAppear = new Event(WrapTag("PanelAppear"));

        private static string WrapTag(string nameEvent)
        {
            return typeof(GameShellEvents).Name + "." + nameEvent;
        }
    }

    public class ArrowButtonParams : EventParams
    {
        public ArrowButtonType Type;

        public ArrowButtonParams(ArrowButtonType type)
        {
            Type = type;
        }
    }

    public class LangaugeParams : EventParams
    {
        public string NemLocalId;

        public LangaugeParams(string localId)
        {
            NemLocalId = localId;
        }
    }

    public class VolumeParams : EventParams
    {
        public float Value;

        public VolumeParams(float value)
        {
            Value = value;
        }
    }
}
