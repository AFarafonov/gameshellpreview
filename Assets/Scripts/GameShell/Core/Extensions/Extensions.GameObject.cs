﻿using UnityEngine;

namespace GameShell.Core.Extensions
{
    public static partial class Extension
    {
        public static GameObject AddChild( this GameObject parent, GameObject child )
        {
            child.transform.SetParent( parent.transform );
            return child;
        }

        public static GameObject AddChild( this GameObject parent, Transform child )
        {
            child.SetParent( parent.transform );
            return child.gameObject;
        }

        public static Transform AddChild( this Transform parent, GameObject child )
        {
            child.transform.SetParent( parent );
            return child.transform;
        }

        public static Transform AddChild(this Transform parent, Transform child)
        {
            child.SetParent(parent.transform);
            return child;
        }

        public static GameObject AddChildUI(this GameObject parent, GameObject child)
        {
            child.transform.SetParent(parent.transform, false);
            return child;
        }

        public static GameObject AddChildUI(this RectTransform parent, GameObject child)
        {
            child.transform.SetParent(parent, false);
            return child;
        }

        public static void ResetLocalIgnoreScale( this GameObject go )
        {
            go.transform.localPosition = Vector3.zero;
            go.transform.localRotation = Quaternion.identity;
        }

        public static void ResetLocal( this GameObject go )
        {
            go.transform.localPosition = Vector3.zero;
            go.transform.localRotation = Quaternion.identity;
            go.transform.localScale = Vector3.one;
        }

        public static void ResetScale( this GameObject go )
        {
            go.transform.localScale = Vector3.one;
        }

        public static void Reset( this GameObject go )
        {
            go.transform.position = Vector3.zero;
            go.transform.rotation = Quaternion.identity;
            go.transform.localScale = Vector3.one;
        }

        public static void ResetUI( this GameObject go )
        {
            go.transform.position = Vector3.zero;
            go.transform.rotation = Quaternion.identity;
            go.transform.localScale = Vector3.one;

            var rectTrans = go.GetComponent< RectTransform >();
            rectTrans.anchoredPosition = Vector2.zero;
            rectTrans.localRotation = Quaternion.identity;
        }

        public static void SetLayerRecursive( this GameObject go, int layer )
        {
            go.layer = layer;
            var count = go.transform.childCount;
            for( var i = 0; i < count; i++ )
            {
                var transform = go.transform.GetChild( i );
                transform.gameObject.layer = layer;

                if ( transform.childCount > 0 )
                    SetLayerRecursive( transform.gameObject, layer );
            }
        }

        public static void SetStaticRecursive( this GameObject go )
        {
            go.isStatic = true;
            for( var i = 0; i < go.transform.childCount; ++i )
            {
                go.transform.GetChild( i ).gameObject.SetStaticRecursive();
            }
        }

        public static string GetPath( this GameObject go )
        {
            var path = "/" + go.name;

            while( go.transform.parent != null )
            {
                go = go.transform.parent.gameObject;
                path = "/" + go.name + path;
            }

            return path;
        }

        public static T AddGetComponent< T >( this Transform transform ) where T : Component
        {
            return transform.gameObject.AddOrGetComponent< T >();
        }

        public static T AddOrGetComponent< T >( this GameObject gameObject ) where T : Component
        {
            T component = gameObject.GetComponent< T >();
            if ( component == null )
            {
                component = gameObject.AddComponent< T >();
            }
            return component;
        }

        public static void SetSize( this RectTransform trans, Vector2 newSize )
        {
            Vector2 oldSize = trans.rect.size;
            Vector2 deltaSize = newSize - oldSize;
            trans.offsetMin = trans.offsetMin - new Vector2( deltaSize.x * trans.pivot.x, deltaSize.y * trans.pivot.y );
            trans.offsetMax = trans.offsetMax + new Vector2( deltaSize.x * ( 1f - trans.pivot.x ), deltaSize.y * ( 1f - trans.pivot.y ) );
        }

        public static void SetWidth( this RectTransform trans, float newSize )
        {
            SetSize( trans, new Vector2( newSize, trans.rect.size.y ) );
        }

        public static void SetHeight( this RectTransform trans, float newSize )
        {
            SetSize( trans, new Vector2( trans.rect.size.x, newSize ) );
        }

        public static GameObject FindChildByNameRecursive( this GameObject go, string name )
        {
            var child = go.transform.Find( name );
            if ( child != null )
            {
                return child.gameObject;
            }

            for( var idx = 0; idx < go.transform.childCount; ++idx )
            {
                var found = go.transform.GetChild( idx ).gameObject.FindChildByNameRecursive( name );
                if ( found != null )
                {
                    return found;
                }
            }
            return null;
        }

        public static GameObject FindChildByTagRecursive( this GameObject go, string tag )
        {
            for( var idx = 0; idx < go.transform.childCount; ++idx )
            {
                var child = go.transform.GetChild( idx );
                if ( child.CompareTag( tag ) )
                {
                    return child.gameObject;
                }
            }
            for( var idx = 0; idx < go.transform.childCount; ++idx )
            {
                var found = go.transform.GetChild( idx ).gameObject.FindChildByTagRecursive( tag );
                if ( found != null )
                {
                    return found;
                }
            }
            return null;
        }
    }
}