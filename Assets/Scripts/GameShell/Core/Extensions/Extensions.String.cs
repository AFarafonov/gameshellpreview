﻿namespace GameShell.Core.Extensions
{
    public static partial class Extension
    {
        public static string ToLocalization( this string stringId)
        {
            return (GS.Instance.Settings.EnableLocalization)? GS.Instance.Localization.GetString(stringId) : stringId;
        }
    }
}