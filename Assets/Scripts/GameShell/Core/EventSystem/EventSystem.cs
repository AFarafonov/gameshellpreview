﻿using System.Collections.Generic;

namespace GameShell.Core.EventSystem
{
    public class EventSystem
    {
        public delegate void EventCallback(EventParams param);
        private readonly Dictionary<Event, HashSet<EventCallback>> _categories = new Dictionary<Event, HashSet<EventCallback>>();

        public void Attach(Event category, EventCallback callback)
        {
            InnerAttach(category, callback);
        }

        public void Detach(Event category, EventCallback callback)
        {
            InnerDetach(category, callback);
        }

        public void Call(Event category)
        {
            InnerCall(category);
        }

        public void Call(Event category, EventParams param)
        {
            InnerCall(category, param);
        }

        private void InnerAttach(Event category, EventCallback callback)
        {
            HashSet<EventCallback> value;
            if (_categories.TryGetValue(category, out value))
                value.Add(callback);
            else
            {
                value = new HashSet<EventCallback> { callback };
                _categories.Add(category, value);
            }
        }

        private void InnerDetach(Event category, EventCallback callback)
        {
            HashSet<EventCallback> value;
            if (!_categories.TryGetValue(category, out value)) return;
            value.Remove(callback);
        }

        private void InnerCall(Event category)
        {
            HashSet<EventCallback> value;
            if (!_categories.TryGetValue(category, out value)) return;

            foreach (var callback in value)
                callback(null);
        }

        private void InnerCall(Event category, EventParams param)
        {
            HashSet<EventCallback> value;
            if (!_categories.TryGetValue(category, out value)) return;

            foreach (var callback in value)
                callback(param);
        }
    }
}
