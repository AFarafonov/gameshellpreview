﻿using System;
using System.Collections.Generic;

namespace GameShell.Core
{
    public class StateMachine<T>  where T : struct
    {
        #region internal class State
        internal class State
        {
#pragma warning disable 67
            event Action OnLeave;
#pragma warning restore 67

            event Action<object> OnEnter;
            event Action<object> OnUpdate;

            public State(Action<object> onEnter, Action onLeave, Action<object> onUpdate)
            {
                OnEnter += onEnter;
                OnLeave += onLeave;
                OnUpdate += onUpdate;
            }
            
            public void CallOnLeave()
            {
                if(OnLeave != null)
                    OnLeave();
            }

            public void CallOnEnter(object obj = null)
            {
                if (OnEnter != null)
                    OnEnter(obj);
            }

            public void CallOnUpdate(object obj = null)
            {
                if (OnUpdate != null)
                    OnUpdate(obj);
            }

            public void Clear()
            {
                OnEnter = null;
                OnLeave = null;
                OnUpdate = null;
            }
        }
        #endregion

        public delegate void StateChange(T obj);
        public StateChange OnAfterChangeState;
        public StateChange OnBeforeChangeState;

        public T CurrentState { get; private set; }
        readonly Dictionary<T, State> _states = new Dictionary<T, State>();

        public StateMachine()
        {
        }

        public StateMachine(T startState)
        {
            CurrentState = startState;
        }

        public bool Check(T state)
        {
            return CurrentState.Equals(state);
        }

        public void Add(T state, Action<object> onEnter = null, Action onLeave = null, Action<object> onUpdate = null)
        {
            var newState = new State(onEnter, onLeave, onUpdate);
            _states.Add(state, newState);
        }

        State Get(T state)
        {
            if (!_states.ContainsKey(state))
                throw new Exception("State machine don't have " + state + " condition");
            return _states[state];
        }

        public void Remove(T state)
        {
            State removeState = Get(state);
            if (CurrentState.Equals(state))
            {
                removeState.CallOnLeave();
                removeState.Clear();
            }
            _states.Remove(state);
        }

        public void Clear(bool leaveSignal = false)
        {
            if (_states.Count == 0) return;

            if (_states.ContainsKey(CurrentState) && leaveSignal)
                _states[CurrentState].CallOnLeave();
            
            foreach (var state in _states.Values)
                state.Clear();
            
            _states.Clear();
        }
        
        public void SetState(T state, object obj = null)
        {
            if (_states.ContainsKey(CurrentState))
            {
                if (CurrentState.Equals(state))
                {
                    _states[CurrentState].CallOnUpdate(obj);
                    return;
                }
                _states[CurrentState].CallOnLeave();
            }

            CurrentState = state;

            if (_states.ContainsKey(CurrentState))
            {
                if(OnBeforeChangeState != null)
                    OnBeforeChangeState(CurrentState);

                _states[CurrentState].CallOnEnter(obj);

                if (OnAfterChangeState != null)
                    OnAfterChangeState(CurrentState);
            }
        }

        ~StateMachine()
        {
            Clear();
        }
    }
}