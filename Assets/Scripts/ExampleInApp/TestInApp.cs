﻿using UnityEngine;
using GameShell;
using GameShell.Buttons;
using GameShell.Core.EventSystem;
using GameShell.Dialogs;

public class TestInApp : MonoBehaviour
{

    private bool _isInit;
    private ButtonComponent _button;

    private void OnPlayGame(EventParams param)
    {
        var go = (GameObject)Instantiate(Resources.Load("ExampleInApp/ButtonShopTest"));
        _button = new ButtonComponent(GS.Instance.GameContainer, go, OnClick);
        _button.Init();
    }


    private void OnClick()
    {
        Debug.Log("On Clcik");

        var parentControlDialog = new ShopDialog(GS.Instance.DialogsContainer, GS.Instance.Settings.ShopDialog);
        parentControlDialog.Init();
    }

    void Update () {

        if (!_isInit && GS.Instance != null && GS.Instance.EventSystem != null)
        {
            Init();
        }
	}

    private void Init()
    {
        GS.Instance.EventSystem.Attach(GameShellEvents.ClickPlayGame, OnPlayGame);
        _isInit = true;
    }
}
